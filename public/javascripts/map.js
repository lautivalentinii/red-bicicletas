var map = L.map('main_map').setView([-31.384043, -64.183793], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);


$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function (result) {
        console.log(result);
        result.bicicleta.forEach(bici => {
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        })
    }
})